var Board = React.createFactory(window.Board);
var Color = window.Color;
var FigureType = window.FigureType;
var Figures = window.Figures;

ReactDOM.render(
    React.createElement("h1", {"id": "pageTitle"}, "Chessboard TEST"),
    document.getElementById('titleContainer')
);


setTimeout(function () {
    var helloElement = document.getElementById("pageTitle");
    helloElement.textContent = "Let's Begin!";
    ReactDOM.render(Board(boardContent), document.getElementById("boardContainer"));
}, 1500);


var boardContent = {
    cols: ["A", "B", "C", "D", "E", "F", "G", "H"],
    rows: [{
        "A": Figures.BLACK.ROOK,
        "B": Figures.BLACK.KNIGHT,
        "C": Figures.BLACK.BISHOP,
        "D": Figures.BLACK.QUEEN,
        "E": Figures.BLACK.KING,
        "F": Figures.BLACK.BISHOP,
        "G": Figures.BLACK.KNIGHT,
        "H": Figures.BLACK.ROOK
        },
        {
            "A": Figures.BLACK.PAWN,
            "B": Figures.BLACK.PAWN,
            "C": Figures.BLACK.PAWN,
            "D": Figures.BLACK.PAWN,
            "E": Figures.BLACK.PAWN,
            "F": Figures.BLACK.PAWN,
            "G": Figures.BLACK.PAWN,
            "H": Figures.BLACK.PAWN
        },
        {"A": "", "B": "", "C": "", "D": "", "E": "", "F": "", "G": "", "H": ""},
        {"A": "", "B": "", "C": "", "D": "", "E": "", "F": "", "G": "", "H": ""},
        {"A": "", "B": "", "C": "", "D": "", "E": "", "F": "", "G": "", "H": ""},
        {"A": "", "B": "", "C": "", "D": "", "E": "", "F": "", "G": "", "H": ""},
        {
            "A": Figures.WHITE.PAWN,
            "B": Figures.WHITE.PAWN,
            "C": Figures.WHITE.PAWN,
            "D": Figures.WHITE.PAWN,
            "E": Figures.WHITE.PAWN,
            "F": Figures.WHITE.PAWN,
            "G": Figures.WHITE.PAWN,
            "H": Figures.WHITE.PAWN
        },
        {
            "A": Figures.WHITE.ROOK,
            "B": Figures.WHITE.KNIGHT,
            "C": Figures.WHITE.BISHOP,
            "D": Figures.WHITE.QUEEN,
            "E": Figures.WHITE.KING,
            "F": Figures.WHITE.BISHOP,
            "G": Figures.WHITE.KNIGHT,
            "H": Figures.WHITE.ROOK
        }]
};


function getFigureInCell(column, row) {
    var cell = document.getElementById(column+row);
    var result = cell.textContent;
    return result;
}

function addToMovesIfNoFigureInCell(moves, column, row) {
    var figure = getFigureInCell(column, row);
    if (figure == "") {
        moves.push([column, row])
    }
    return moves;
}



function calculatePossibleMoves(figureType, color, startColumn, startRow) {
    var result = [];
    switch (figureType) {
        case FigureType.PAWN:
            if (color == Color.WHITE) {
                addToMovesIfNoFigureInCell(result, startColumn, startRow + 1);
                if (startRow == 2) {
                    addToMovesIfNoFigureInCell(result, startColumn, startRow + 2);
                }
                
            }
            break;
        case FigureType.KNIGHT:
            break;
        case FigureType.BISHOP:
            break;
        case FigureType.ROOK:
            break;
        case FigureType.QUEEN:
            break;
        case FigureType.KING:
            break;
    }
        
    return result;
}


function isValidMove(figureType, color, startColumn, startRow, endColumn, endRow) {
    var result = false;

    var possibleMoves = calculatePossibleMoves(figureType, color, startColumn, startRow);
    //calculate possible end positions from start position for figure
    // if (endRow,endColumn) is in possible end positions return true
    return result;
}


