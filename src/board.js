window.Board = React.createClass({

    render: function render() {
        var _self = this;
        var thead = React.DOM.thead({}, _self.props.cols.map(function(col) {
            return React.DOM.th({}, col);
        }));
        var tRows = this.props.rows.map(function (row) {
            return React.DOM.tr({},
                _self.props.cols.map(function (col) {
                    return React.DOM.td({}, row[col] || "");
                }));
        });
        var tbody = React.DOM.tbody({},[tRows]);
        return React.DOM.table({"id":"board"}, [thead, tbody]);
    }
});