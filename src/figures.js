PAWN = "pawn";
KNIGHT = "knight";
BISHOP = "bishop";
ROOK = "rook";
QUEEN = "queen";
KING = "king";

WHITE = "white";
BLACK = "black";

Figures = {
    WHITE: {PAWN: "\u2659", KNIGHT: "\u2658", BISHOP: "\u2657", ROOK: "\u2656", QUEEN: "\u2655", KING: "\u2654"},
    BLACK: {PAWN: "\u265F", KNIGHT: "\u265E", BISHOP: "\u265D", ROOK: "\u265C", QUEEN: "\u265B", KING: "\u265A"}
};


window.Figures = Figures;
window.FigureType = {PAWN: PAWN, KNIGHT: KNIGHT, BISHOP:BISHOP, ROOK:ROOK, QUEEN:QUEEN, KING:KING}